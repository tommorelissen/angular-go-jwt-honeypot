module mymodule

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/motemen/go-loghttp v0.0.0-20170804080138-974ac5ceac27
	github.com/motemen/go-nuts v0.0.0-20210915132349-615a782f2c69 // indirect
	github.com/rs/cors v1.8.0
)
